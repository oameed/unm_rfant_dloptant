#! /bin/bash

source  activate tf2py
cd      run/gan

echo ' PROCESSING RFANT3 DATASET FOR CONDITIONAL TRAINING'
python preprocessing.py -data rfant3 -pt 1
echo ' CREATING PROJECT DIRECTORY '                            'v11'
rm     -rf                                       ../../networks/v11
tar    -xzf  ../../networks/v00.tar.gz -C        ../../networks
mv           ../../networks/v00                  ../../networks/v11

echo ' TRAINING CONDITIONAL GAN ON RFANT3 '
python train_cgan.py    -data rfant3 -pt 1 -net v11 -sc 3 -b 128 -epc 125

echo ' GENERATING PREDICTIONS '
python predict.py       -data rfant3 -pt 1 -net v11


