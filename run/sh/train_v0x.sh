#! /bin/bash

source  activate tf2py
cd      run/gan

echo ' CREATING PROJECT DIRECTORY '                            'v01'
rm     -rf                                       ../../networks/v01
tar    -xzf  ../../networks/v00.tar.gz -C        ../../networks
mv           ../../networks/v00                  ../../networks/v01

echo ' CREATING PROJECT DIRECTORY '                            'v02'
rm     -rf                                       ../../networks/v02
tar    -xzf  ../../networks/v00.tar.gz -C        ../../networks
mv           ../../networks/v00                  ../../networks/v02

echo ' TRAINING             GAN ON MNIST '
python train_gan.py     -data MNIST  -pt 0 -net v01 -sc 1 -b 128 -epc 50

echo ' TRAINING CONDITIONAL GAN ON MNIST '
python train_cgan.py    -data MNIST  -pt 1 -net v02 -sc 1 -b 128 -epc 125

echo ' GENERATING PREDICTIONS '
python predict.py       -data MNIST  -pt 0 -net v01
python predict.py       -data MNIST  -pt 1 -net v02


