#! /bin/bash

source  activate tf2py
cd      run/gan

echo ' PROCESSING RFANT1 DATASET '
python preprocessing.py -data rfant1 -pt 0
echo ' PROCESSING RFANT2 DATASET '
python preprocessing.py -data rfant2 -pt 0

echo ' CREATING PROJECT DIRECTORY '                            'v21'
rm     -rf                                       ../../networks/v21
tar    -xzf  ../../networks/v00.tar.gz -C        ../../networks
mv           ../../networks/v00                  ../../networks/v21
echo ' CREATING PROJECT DIRECTORY '                            'v22'
rm     -rf                                       ../../networks/v22
tar    -xzf  ../../networks/v00.tar.gz -C        ../../networks
mv           ../../networks/v00                  ../../networks/v22

echo ' TRAINING GAN ON RFANT1 '
python train_gan.py     -data rfant1  -pt 0 -net v21 -sc 3 -b 128 -epc 25
echo ' TRAINING GAN ON RFANT2 '
python train_gan.py     -data rfant2  -pt 0 -net v22 -sc 3 -b 128 -epc 25

echo ' GENERATING PREDICTIONS '
python predict.py       -data rfant1  -pt 0 -net v21
python predict.py       -data rfant2  -pt 0 -net v22


