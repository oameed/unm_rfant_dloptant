%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO       %%%
%%% RF & ANTENNA LAB               %%%
%%% GANs FOR DEVICE OPTIMIZATION   %%%
%%% PREDICTION ACCURACY ASSESSMENT %%%
%%% by: OAMEED NOAKOASTEEN         %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

function predaccuracy(VER,FILENAME,NUM)
clc

FILENAMES={};
for index=1:size(VER,2)
    FILENAMES{index}=fullfile('..','..','networks',VER{index},'predictions',...
                              strcat(FILENAME      ,'.csv')                    );
end
savefilename        =fullfile('..','..','networks',VER{1}    ,'predictions',...
                              strcat('predaccuracy','.svg')                    );

classes             =get_classes         (FILENAMES,NUM)                        ;
[p0,p1]             =calculate_percentage(classes      )                        ;

plotter(classes,[p0,p1],savefilename)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% FUNCTION DEFINITIONS %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%


    function [P0,P1]=calculate_percentage(X)        
        x    =X(:,1);
        count=0     ;
        for i=1:size(x,1)
            if x(i)<= 40
                count=count+1;
            end
        end
        P0=count/size(x,1);

        x    =X(:,2);
        count=0     ;
        for i=1:size(x,1)
            if x(i)> 40 && x(i)< 80 
                count=count+1;
            end
        end
        P1=count/size(x,1);
    end

    function plotter(X,PROB,SAVEFILENAME)
        figure
        fig=gcf;
        fig.Renderer='painters';
        set(gcf,'position',[fig.Position(1),...
                            fig.Position(2),...
                            fig.Position(3),...
                            fig.Position(3)    ])
        hold

        loc1=1  ;
        loc2=1.5;

        scatter(loc1.*ones(size(X,1),1),X(:,1),'b')
        scatter(loc2.*ones(size(X,1),1),X(:,2),'r')
        
        xlim([0.75 1.75])
        ylim([0    100 ])

        set(gca,'xtick'     ,([loc1,loc2]),...
                'xticklabel',({'0' ,'1' }),...
                'ytick'     ,([ 40 , 80 ]),...
                'yticklabel',({'40','80'})    )

        xlim([0.75 1.75])
        box
        title({['label 0 accuracy : ' num2str(PROB(1)*100) ' %'],...
               ['label 1 accuracy : ' num2str(PROB(2)*100) ' %']    })
        grid
        saveas(gca,SAVEFILENAME,'svg')
        close all
    end

    function classes=get_classes(FILENAMES,NUM)
        predictions=[];
        for i=1:size(FILENAMES,2)
            predictions=[predictions;readmatrix(FILENAMES{i})];
        end
        classes    =zeros(NUM,size(predictions,1)/NUM)   ;
        for i=1:size(classes,2)
            classes(:,i)=predictions((i-1)*NUM+1:i*NUM,1);
        end
    end

end


