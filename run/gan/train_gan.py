####################################
### UNIVERSITY OF NEW MEXICO     ###
### RF & ANTENNA LAB             ###
### GANs FOR DEVICE OPTIMIZATION ###
### TRAINING/TESTING             ###
### by: OAMEED NOAKOASTEEN       ###
####################################

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import sys
sys.path.append(os.path.join(sys.path[0],'..','..','lib'         ))
sys.path.append(os.path.join(sys.path[0],'..','..','lib','models'))
import numpy             as np
import tensorflow        as tf
from   paramd     import    PATHS, PARAMS 
from   inputd     import   (saveTFRECORDS          , 
                            inputTFRECORDS          )
from   utilsd     import   (plotter                ,
                            plot_to_image          ,
                            wHDF                    )
from   nndGAN     import   (callback_custom_ckpt   ,
                            callback_custom_monitor,
                            callback_custom_history,
                            def_discriminator      ,
                            def_generator          ,
                            z_sample               ,
                            loss_wasserstein_disc  ,
                            loss_wasserstein_genr  ,
                            GAN                     )

tfr_train     =os.path.join(PATHS[3],'train'       )
tfr_validation=os.path.join(PATHS[3],'validation'  )
log_train     =os.path.join(PATHS[5],'train'       )
Z             =z_sample    ([100    , PARAMS[0][7]])

#############
### MODEL ###
#############
inputs_disc  =tf.keras.Input   (shape =[np.prod(PARAMS[1])],                       name='disc_input'   )
outputs_disc =def_discriminator(inputs_disc                                                            )
discriminator=tf.keras.Model   (inputs=inputs_disc         , outputs=outputs_disc, name='discriminator')

inputs_genr  =tf.keras.Input   (shape =PARAMS[0][7 ]       ,                       name='genr_input'   )
outputs_genr =def_generator    (inputs_genr                , np.prod(PARAMS[1])                        )
generator    =tf.keras.Model   (inputs=inputs_genr         , outputs=outputs_genr, name='generator'    )

model        =GAN(discriminator= discriminator,
                  generator    = generator    ,
                  params       = PARAMS       ,
                  paths        = PATHS         )

model.compile    (d_optimizer  = tf.keras.optimizers.Adam(learning_rate=1e-4), 
                  g_optimizer  = tf.keras.optimizers.Adam(learning_rate=1e-4), 
                  loss_disc    = loss_wasserstein_disc                       ,
                  loss_genr    = loss_wasserstein_genr                        )

#################
### CALLBACKS ###
#################
callbacks_train=[callback_custom_ckpt          ()                                                   ,
                 callback_custom_monitor       (writer   =tf.summary.create_file_writer(log_train),
                                                plotter  =plotter                                 ,
                                                converter=plot_to_image                            ),
                 callback_custom_history       (plotter  =plotter                                 ,
                                                whdf     =wHDF                                     ),
                 tf.keras.callbacks.TensorBoard(log_dir       =log_train                            , 
                                                histogram_freq=1                                    , 
                                                update_freq   ='batch'                               )]

#############
### TRAIN ###
#############
print        (' WRITING  TRAINING   DATA TO TFRECORDS FORMAT ')
saveTFRECORDS(PATHS, PARAMS, 'train')

print        (' FITTING  MODEL'                               )
data_train     =inputTFRECORDS(tfr_train, PARAMS)
data_validation=              (Z        ,Z      )
model.fit    (x                    =data_train     ,
              epochs               =PARAMS[0][4]   ,
              validation_data      =data_validation,
              validation_batch_size=100            ,              
              callbacks            =callbacks_train,
              verbose              =1                     )
print        (' TRAINING FINISHED '                       )


