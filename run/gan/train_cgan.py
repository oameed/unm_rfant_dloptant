####################################
### UNIVERSITY OF NEW MEXICO     ###
### RF & ANTENNA LAB             ###
### GANs FOR DEVICE OPTIMIZATION ###
### TRAINING/TESTING             ###
### by: OAMEED NOAKOASTEEN       ###
####################################

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import sys
sys.path.append(os.path.join(sys.path[0],'..','..','lib'         ))
sys.path.append(os.path.join(sys.path[0],'..','..','lib','models'))
import numpy             as np
import tensorflow        as tf
from   paramd     import    PATHS, PARAMS 
from   inputd     import   (saveTFRECORDS          , 
                            inputTFRECORDS          )
from   utilsd     import   (plotter                ,
                            plot_to_image          ,
                            embedding_visulaizer   ,
                            wHDF                    )
from   nndCGAN    import   (callback_custom_ckpt   ,
                            callback_custom_monitor,
                            callback_custom_history,
                            def_embedding          ,
                            def_discriminator      ,
                            def_generator          ,
                            z_sample               ,
                            loss_wasserstein_disc  ,
                            loss_wasserstein_genr  ,
                            GAN                     )

tfr_train     =os.path.join(PATHS[3],'train'             )
tfr_validation=os.path.join(PATHS[3],'validation'        )
log_train     =os.path.join(PATHS[5],'train'             )

Z, Z_lab      =z_sample    ([PARAMS[2][0]**2, PARAMS[0][7]], PARAMS[2][0], True)

#############
### MODEL ###
#############
embd_shape         =50
inputs_embd        =tf.keras.Input   (shape =[]                                    ,                       name='embd_input'        )
outputs_embd       =def_embedding    (inputs_embd, embd_shape, PARAMS[2][0]                                                         )
embedding          =tf.keras.Model   (inputs=inputs_embd                           , outputs=outputs_embd, name='embedding'         )

inputs_disc_img    =tf.keras.Input   (shape =[np.prod(PARAMS[1])]                  ,                       name='disc_input_img'    )
inputs_disc_img_lab=tf.keras.Input   (shape =[embd_shape]                          ,                       name='disc_input_img_lab')
outputs_disc       =def_discriminator(inputs_disc_img, inputs_disc_img_lab         ,                                                )
discriminator      =tf.keras.Model   (inputs=[inputs_disc_img, inputs_disc_img_lab], outputs=outputs_disc, name='discriminator'     )

inputs_genr_Z      =tf.keras.Input   (shape =         PARAMS[0][7]                 ,                       name='genr_input_Z'      ) 
inputs_genr_Z_lab  =tf.keras.Input   (shape =[embd_shape]                          ,                       name='genr_input_Z_lab'  )
outputs_genr       =def_generator    (inputs_genr_Z  , inputs_genr_Z_lab           , np.prod(PARAMS[1])                             )
generator          =tf.keras.Model   (inputs=[inputs_genr_Z  , inputs_genr_Z_lab  ], outputs=outputs_genr, name='generator'         )

model              =GAN(embedding    = embedding    ,
                        discriminator= discriminator,
                        generator    = generator    ,
                        params       = PARAMS       ,
                        paths        = PATHS         )

model.compile          (d_optimizer  = tf.keras.optimizers.RMSprop(learning_rate=5e-5),  # tf.keras.optimizers.Adam(learning_rate=1e-4) 
                        g_optimizer  = tf.keras.optimizers.RMSprop(learning_rate=5e-5),  # tf.keras.optimizers.Adam(learning_rate=1e-4) 
                        loss_disc    = loss_wasserstein_disc                          ,
                        loss_genr    = loss_wasserstein_genr                           ) # ATTENTION !!!       RFANT3 DATASET IS TRAINED WITH ADAM 
                                                                                         #               WHILE MNIST  DATASET IS TRAINED WITH RMSPROP

#################
### CALLBACKS ###
#################
callbacks_train=[callback_custom_ckpt          ()                                                        ,
                 callback_custom_monitor       (writer        =tf.summary.create_file_writer(log_train),
                                                plotter       =plotter                                 ,
                                                converter     =plot_to_image                           ,
                                                emdvis        =embedding_visulaizer                     ),
                 callback_custom_history       (plotter       =plotter                                 ,
                                                whdf          =wHDF                                     ),
                 tf.keras.callbacks.TensorBoard(log_dir       =log_train                               , 
                                                histogram_freq=1                                       , 
                                                update_freq   ='batch'                                  ) ]

#############
### TRAIN ###
#############
print        (' WRITING  TRAINING   DATA TO TFRECORDS FORMAT ')
saveTFRECORDS(PATHS, PARAMS, 'train')

print        (' FITTING  MODEL'                               )
data_train     =inputTFRECORDS(tfr_train, PARAMS   )

data_validation=              ([Z,Z_lab], [Z,Z_lab])

model.fit    (x                    =data_train     ,
              epochs               =PARAMS[0][4]   ,
              validation_data      =data_validation,
              validation_batch_size=PARAMS[2][0]**2,              
              callbacks            =callbacks_train,
              verbose              =1                     )
print        (' TRAINING FINISHED '                       )


