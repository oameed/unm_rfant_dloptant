####################################
### UNIVERSITY OF NEW MEXICO     ###
### RF & ANTENNA LAB             ###
### GANs FOR DEVICE OPTIMIZATION ###
### PRE-PROCESSING DATASETS      ###
### by: OAMEED NOAKOASTEEN       ###
####################################


import                    os
import                    sys
sys.path.append(os.path.join(sys.path[0],'..','..','lib'))
import numpy           as np
from   paramd   import    PATHS, PARAMS
from   datasetd import    write_dataset
from   utilsd   import    cleanup

cleanup      (os.path.join(PATHS[0],'uncompressed'))
cleanup      (os.path.join(PATHS[1]               ))
cleanup      (os.path.join(PATHS[2]               ))

write_dataset(PARAMS,PATHS                         )

cleanup      (os.path.join(PATHS[0],'uncompressed'))


