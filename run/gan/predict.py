####################################################
### UNIVERSITY OF NEW MEXICO                     ###
### RF & ANTENNA LAB                             ###
### GANs FOR DEVICE OPTIMIZATION                 ###
### GENERATING PREDICTIONS USING A TRAINED MODEL ###
### by: OAMEED NOAKOASTEEN                       ###
####################################################

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import sys
sys.path.append(os.path.join(sys.path[0],'..','..','lib'         ))
sys.path.append(os.path.join(sys.path[0],'..','..','lib','models'))
import numpy                    as  np
import tensorflow               as  tf
from   matplotlib import pyplot as  plt
from   paramd     import            PATHS, PARAMS
from   utilsd     import           (wHDF                    ,
                                    rCSV                    ,
                                    rHDF                    ,
                                    wCSV                    ,
                                    plotter                 ,
                                    scale_vector_features   ,
                                    scale_up_vector_features )

datafilename=os.path.join(PATHS[1], PARAMS[0][0] )
savefilename=os.path.join(PATHS[6],'predictions' )
model       =tf.keras.models.load_model(PATHS [4])                                  # LOAD MODEL

if PARAMS[0][0] in ['rfant3']:
 from nndCGAN import z_sample
 sample_size=100*PARAMS[2][0]
 Z, Z_lab   =z_sample([sample_size, PARAMS[0][7]], PARAMS[2][0], True)
 inputs     =[Z, Z_lab]                                                             # LOAD INPUTS
 Z_lab_feat =model.get_layer('embedding')( inputs[1]            )   
 x          =model.get_layer('generator')([inputs[0],Z_lab_feat]).numpy()           # GENERATE PREDICTIONS
 minvals    =rCSV(PATHS[2],'scale'+'_min'+'.csv')
 maxvals    =rCSV(PATHS[2],'scale'+'_max'+'.csv')
 predictions=scale_up_vector_features(x,[minvals,maxvals])                          # SCALE UP
 wHDF(savefilename+'.h5',
      ['predictions']   ,
      [ predictions ]    )                                                          # EXPORT   SCALED-UP PREDICTIONS AS HDF 
 wCSV(savefilename+'.csv', 
       predictions        )                                                         # EXPORT   SCALED-UP PREDICTIONS AS TXT
 fobj,keys  =rHDF                 (datafilename+'.h5'              )
                                                                                    # EXPORT   SCALED-UP PREDICTIONS AS PLOT
 figure     =plotter              ('box', [predictions,fobj['features'][:,:-1]],[])
 plt.savefig                      (savefilename+'_scaled_up'+'.png',format='png'        )
else:
 if  PARAMS[0][0] in ['MNIST']:
  if PARAMS[0][9] in ['gan'  ]:
   from nndGAN import z_sample
   Z          =z_sample([100, PARAMS[0][7]])
   predictions=model(Z).numpy()
   predictions=np.reshape(predictions, (-1,PARAMS[1][0],PARAMS[1][1]))
   wHDF(savefilename+'.h5',
        ['predictions']   ,
        [ predictions ]    )
   figure     =plotter   ('collage'  , predictions, ['gray']         )
   plt.savefig           (savefilename+'.png',format='png'           )
  else:
   if PARAMS[0][9] in ['cgan']:
    from nndCGAN import z_sample
    sample_size=10*PARAMS[2][0]
    Z, Z_lab   =z_sample([sample_size, PARAMS[0][7]], PARAMS[2][0], True)
    inputs     =[Z, Z_lab]                                                   
    Z_lab_feat =model.get_layer('embedding')( inputs[1]            )   
    predictions=model.get_layer('generator')([inputs[0],Z_lab_feat]).numpy()
    predictions=np.reshape(predictions, (-1,PARAMS[1][0],PARAMS[1][1]))
    wHDF(savefilename+'.h5',
         ['predictions']   ,
         [ predictions ]    )
    figure     =plotter   ('collage'  , predictions, ['gray']         )
    plt.savefig           (savefilename+'.png',format='png'           )
 else:
  if PARAMS[0][0] in ['rfant1','rfant2']:
   from nndGAN import z_sample
   Z          =z_sample([50,PARAMS[0][7]])
   x          =model(Z).numpy()
   minvals    =rCSV(PATHS[2],'scale'+'_min'+'.csv')
   maxvals    =rCSV(PATHS[2],'scale'+'_max'+'.csv')
   predictions=scale_up_vector_features(x,[minvals,maxvals])                        # SCALE UP
   wHDF(savefilename+'.h5' ,
        ['predictions']    ,
        [ predictions ]     )                                                       # EXPORT   SCALED-UP PREDICTIONS AS HDF 
   wCSV(savefilename+'.csv',  
        predictions         )                                                       # EXPORT   SCALED-UP PREDICTIONS AS TXT
   fobj,keys  =rHDF   (datafilename+'.h5'                             )
   figure     =plotter('box', [predictions,fobj['features'][:,:-1]],[])             # EXPORT   SCALED-UP PREDICTIONS AS PLOT
   plt.savefig        (savefilename+'_scaled_up'+'.png',format='png'  )


