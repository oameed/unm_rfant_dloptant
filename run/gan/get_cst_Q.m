%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% UNIVERSITY OF NEW MEXICO                 %%%
%%% RF & ANTENNA LAB                         %%%
%%% GANs FOR DEVICE OPTIMIZATION             %%%
%%% RUNNING CST SIMULATIONS USING PREDICTION %%%
%%% by: Jayakrishnan Vijayamohanan           %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

close all
clear
clc


M       =csvread('predictions.csv');
Output  =zeros (size(M,1),1       );

for i=1:size(M,1)
    
    CST   =actxserver('CSTStudio.Application');
    mws   =CST.invoke('Active3D'             );
    solv  =mws.invoke('Solver'               );
    plot1d=mws.invoke('Plot1d'               );
    export=mws.invoke('ASCIIExport'          );
    
    mws.invoke('StoreDoubleParameter','X2'  ,M(i,1 ));
    mws.invoke('StoreDoubleParameter','X3'  ,M(i,2 ));
    mws.invoke('StoreDoubleParameter','X4'  ,M(i,3 ));
    mws.invoke('StoreDoubleParameter','X5'  ,M(i,4 ));
    mws.invoke('StoreDoubleParameter','C2'  ,M(i,5 ));
    mws.invoke('StoreDoubleParameter','C3'  ,M(i,6 ));
    mws.invoke('StoreDoubleParameter','C4'  ,M(i,7 ));
    mws.invoke('StoreDoubleParameter','C5'  ,M(i,8 ));
    mws.invoke('StoreDoubleParameter','SMAG',M(i,9 ));
    mws.invoke('StoreDoubleParameter','FW'  ,M(i,10));

    
    invoke(export,'reset'   );
    mws.invoke(   'Rebuild' );
    
    solv.invoke('Start');
    exportpathr = 'C:\Users\Jay\Documents\MATLAB\Q factor analysis_LPDA\Zreal.txt';
    exportpathi = 'C:\Users\Jay\Documents\MATLAB\Q factor analysis_LPDA\Zimag.txt';
    
    CstExportZparametersrealTXT(mws, exportpathr);
    CstExportZparametersimagTXT(mws, exportpathi);
 
    [ Freq, Zreal] = textread('Zreal.txt', '%f %f','headerlines', 2);
    [ Freq, Zimag] = textread('Zimag.txt', '%f %f','headerlines', 2);
        
    Fref2    =Freq.*10^9               ;
    omega    =2*pi*Fref2               ;
    dr       =diff(Zreal)./diff(Fref2) ;
    dx       =diff(Zimag)./diff(Fref2) ;
         
    cost     = 0; 
    for j=67:1000
        Q(j) =(omega(j)/(2*Zreal(j)))*sqrt((dr(j)^2)+(dx(j)+(abs(dx(j))/omega(j)))^2);
        cost = cost + Q(j)                                                           ;   
    end  
    cost     = cost/1000              ;
    
    Output(i)=cost                    ;
end

csvwrite('classes.csv',Output)

