###############################################
### UNIVERSITY OF NEW MEXICO                ###
### RF & ANTENNA LAB                        ###
### GANs FOR DEVICE OPTIMIZATION            ###
### DATASET PROCESSING FUNCTION DEFINITIONS ###
### by: OAMEED NOAKOASTEEN                  ###
###############################################

import os
import numpy                    as np
import tensorflow               as tf
from   matplotlib import pyplot as plt
from   utilsd     import          (getFILENAMES          ,
                                   tar_uncompress        ,
                                   plotter               ,
                                   wHDF                  ,
                                   rHDF                  ,
                                   check_label_is_correct )

def read_raw_features(PATH):
 data      ={}
 tar_uncompress( getFILENAMES(os.path.join(PATH,'compressed'  ))[0],
                              os.path.join(PATH,'uncompressed')     )
 filenames =     getFILENAMES(os.path.join(PATH,'uncompressed')     )
 for fn in filenames:
  key      =fn.split('_')[1]
  if key in ['btw']:
   indeces  =np.random.randint(0,745,size=486)                                 # BALANCE DATASET SIZE
   data[key]=np.loadtxt(fn, delimiter=',' )[indeces]
  else:
   data[key]=np.loadtxt(fn, delimiter=',' )
 return data

def get_labeled_features(DATA):
 keys     =[i for i in DATA.keys()]
 features =[]
 labels   =[]
 features.append(DATA['btw' ])
 labels.append  ([1 for i in range(DATA['btw' ].shape[0])])
 features.append(DATA['less'])
 labels.append  ([0 for i in range(DATA['less'].shape[0])])
 #for i,key in enumerate(keys):
 # features.append(DATA[key])
 # labels.append  ([i for j in range(DATA[key].shape[0])])
 features=np.concatenate((features[0],features[1]), axis=0)
 labels  =labels[0]+labels[1]
 indeces =[i for i in range(features.shape[0])]
 np.random.shuffle(indeces)
 features=features[indeces]
 labels  =[labels[i] for i in indeces]
 return features, np.array(labels)

def write_dataset(PARAMS,PATHS):
 savefilename    =os.path.join(PATHS[1],PARAMS[0][0]+'.h5' )
 data            =read_raw_features(PATHS[0])
 if  PARAMS[0][9]=='cgan':
  features,labels=get_labeled_features(data)
  wHDF(savefilename         ,
       ['features','labels'],
       [ features , labels ] )
 else:
  if PARAMS[0][9]=='gan' :
   key           =[i for i in data.keys()][0]
   features      =data[key]
   wHDF(savefilename,
       ['features'] ,
       [ features ]  )
  
def get_dataset(TYPE, PATHS, MODE):
 if TYPE    == 'MNIST' :
  if MODE   == 'train' :
   (features, labs), _ =tf.keras.datasets.mnist.load_data()
   features            =np.reshape(features,(-1,np.prod(features.shape[1:])))
   labs                =labs.astype('int64')
 else:
  if TYPE   == 'rfant3':
   if MODE  == 'train' :
    fobj,keys          =rHDF(os.path.join(PATHS[1],TYPE+'.h5'))
    features           =fobj['features'][:]
    labs               =fobj['labels'  ][:]
    check_label_is_correct(features,labs)
    features           =features[:,:-1]                                        # DROP THE LAST COLUMN (Q-FACTOR ASSOCIATED WITH STRUCTURE)
  else:
   if TYPE  in ['rfant1','rfant2']:
    if MODE == 'train'            :
     fobj,keys         =rHDF(os.path.join(PATHS[1],TYPE+'.h5'))
     features          =fobj['features'][:]
     labs              =np.zeros(features.shape[0]).astype('int64')            # DUMMY LABELS
     features          =features[:,:-1]                                        # DROP THE LAST COLUMN (Q-FACTOR ASSOCIATED WITH STRUCTURE)
 return features, labs


