####################################
### UNIVERSITY OF NEW MEXICO     ###
### RF & ANTENNA LAB             ###
### GANs FOR DEVICE OPTIMIZATION ###
### MODEL & CALLBACK DEFINITIONS ###
### by: OAMEED NOAKOASTEEN       ###
####################################

import os
import numpy                    as  np
import tensorflow               as  tf
from   matplotlib import pyplot as  plt
from   utilsd     import           (rHDF                 ,
                                    scale_vector_features )

##############
### LAYERS ###
##############
class clipConstraint(tf.keras.constraints.Constraint):
    
    def __init__(self, value):
     self.value=value
    
    def __call__(self, weights):
      return tf.clip_by_value(weights, -self.value, self.value)

def layers_fc(X,OUTSIZE,NAME,CLIP=False, BATCHNORM=True,LINEAR=False):
 initializer=tf.keras.initializers.he_normal   ()
 if not CLIP:
  x         =tf.keras.layers.Dense             (OUTSIZE                       , 
                                                activation        =None       , 
                                                name              =NAME       ,
                                                kernel_initializer=initializer )(X)
 else:
  clip_value=clipConstraint(0.01)
  x         =tf.keras.layers.Dense             (OUTSIZE                       , 
                                                activation        =None       , 
                                                name              =NAME       ,
                                                kernel_initializer=initializer, 
                                                kernel_constraint =clip_value  )(X)
 if     BATCHNORM:
  x         =tf.keras.layers.BatchNormalization(name              =NAME+'_bn'  )(x)
 if not LINEAR   :
  x         =tf.keras.layers.ReLU              ()                               (x)
 return x

#############
### MODEL ###
#############
def def_embedding(X,SHAPE,NUMCAT):
 initializer=tf.keras.initializers.he_normal   ()
 x          =tf.keras.layers.Embedding         (NUMCAT                            , 
                                                SHAPE                             , 
                                                name                  ='emb_1'    , 
                                                embeddings_initializer=initializer )(X)
 return x

def def_discriminator(X,Y):
 SHAPE      =X.shape[1]
 y          =layers_fc(Y,SHAPE,'flc_1' , False , False , True)
 x          =tf.keras.layers.concatenate([X, y], axis=1      )
 x          =layers_fc(x,SHAPE,'flc_2' , True                )
 x          =layers_fc(x,512  ,'flc_3' , True                )
 x          =layers_fc(x,256  ,'flc_4' , True                )
 x          =layers_fc(x,128  ,'flc_5' , True                )
 x          =layers_fc(x,1    ,'flc_6' , True  , False , True)
 return x

def def_generator(X,Y,SHAPE):
 y          =layers_fc(Y,128  ,'flc_7' , False , False , True)
 x          =layers_fc(X,128  ,'flc_8' ,                     )
 x          =tf.keras.layers.concatenate([x, y], axis=1      )
 x          =layers_fc(x,256  ,'flc_9' ,                     )
 x          =layers_fc(x,512  ,'flc_10',                     )
 x          =layers_fc(x,SHAPE,'flc_11', False , True  , True)
 x          =tf.keras.layers.Lambda(tf.keras.activations.sigmoid)(x)
 return x

def z_sample(SHAPE, NUMCAT, ORDERED=False):
 z      =tf.random.normal (                SHAPE   )
 if not ORDERED:
  z_lab =np.random.randint(0, NUMCAT, size=SHAPE[0]).astype('int64')
  z_lab =tf.constant      (z_lab                   ) 
 else:
  z_lab =[]
  for i in range(NUMCAT):
   z_lab.extend([i for j in range(int(SHAPE[0]/NUMCAT))])
  z_lab=np.array   (z_lab)
  z_lab=tf.constant(z_lab)
 return z, z_lab 

def loss_wasserstein_disc(REAL,FAKE):
 loss_mean_real=tf.reduce_mean(tf.math.multiply(REAL,-tf.ones_like (REAL)))
 loss_mean_fake=tf.reduce_mean(tf.math.multiply(FAKE, tf.ones_like (FAKE)))
 loss          =loss_mean_real+loss_mean_fake
 return loss, loss_mean_real, loss_mean_fake 

def loss_wasserstein_genr(     FAKE):
 loss_mean_fake=tf.reduce_mean(tf.math.multiply(FAKE,-tf.ones_like (FAKE)))
 return loss_mean_fake

class GAN(tf.keras.Model):

    def __init__(self, embedding, discriminator, generator, params, paths):
        super().__init__()
        self.embedding      =embedding
        self.discriminator  =discriminator
        self.generator      =generator
        self.params         =params
        self.paths          =paths

    def compile(self, d_optimizer, g_optimizer, loss_disc, loss_genr):
        super().compile()
        self.d_optimizer    =d_optimizer
        self.g_optimizer    =g_optimizer
        self.loss_disc      =loss_wasserstein_disc
        self.loss_genr      =loss_wasserstein_genr

    def call(self, X, training=None):
        Z         =X[0]
        Z_lab     =X[1]
        Z_lab_feat=self.embedding(Z_lab)
        x         =self.generator([Z,Z_lab_feat], training=True)        
        return x

    def train_step(self, data):
        
        loss_disc_real_temp     =[]
        loss_disc_fake_temp     =[]
        
        images_real, labels_real=data
        
        for i in range(5):
         with tf.GradientTape() as tape_disc:
          Z, Z_lab            =z_sample          ([self.params[0][3], self.params[0][7]], self.params[2][0]                  )
          labels_real_feat    =self.embedding    (labels_real                                                                )
          Z_lab_feat          =self.embedding    (Z_lab                                                                      )
          images_generated    =self.generator    ([Z               , Z_lab_feat      ], training=True                        )
          logit_real          =self.discriminator([images_real     , labels_real_feat], training=True                        )
          logit_fake          =self.discriminator([images_generated, Z_lab_feat      ], training=True                        )
          loss_disc, lmr, lmf =self.loss_disc    (logit_real                          , logit_fake                           )
          loss_disc_real_temp.append(lmr)                                                                                      # DISCRIMINATOR'S LOSS: FROM REAL IMAGES
          loss_disc_fake_temp.append(lmf)                                                                                      # DISCRIMINATOR'S LOSS: FROM FAKE IMAGES
         grads_disc           =tape_disc.gradient(    loss_disc                       , self.discriminator.trainable_weights )
         self.d_optimizer.apply_gradients        (zip(grads_disc                      , self.discriminator.trainable_weights))
        
        with tf.GradientTape() as tape_genr:
         Z, Z_lab            =z_sample           ([self.params[0][3], self.params[0][7]], self.params[2][0]                  )
         Z_lab_feat          =self.embedding     (Z_lab                                                                      )
         images_generated    =self.generator     ([Z               , Z_lab_feat      ], training=True                        )
         logit_fake          =self.discriminator ([images_generated, Z_lab_feat      ], training=True                        )
         loss_genr           =self.loss_genr     (                                      logit_fake                           )
        grads_genr           =tape_genr.gradient (    loss_genr                       , self.generator.trainable_weights     )
        self.g_optimizer.apply_gradients         (zip(grads_genr                      , self.generator.trainable_weights    ))
        
        return {"d_loss_real": tf.reduce_mean(loss_disc_real_temp), 
                "d_loss_fake": tf.reduce_mean(loss_disc_fake_temp),
                "d_loss"     : loss_disc                          ,
                "g_loss"     : loss_genr                           }

##########################
### TRAINING CALLBACKS ###
##########################
class callback_custom_ckpt(tf.keras.callbacks.Callback):    
    
    def on_epoch_end(self, epoch, logs=None):
        tf.keras.models.save_model(self.model ,self.model.paths[4])


class callback_custom_monitor(tf.keras.callbacks.Callback):

    def __init__(self, writer, plotter, converter, emdvis):
        super().__init__()
        self.writer   =writer
        self.plotter  =plotter
        self.converter=converter
        self.emdvis   =emdvis

    def on_epoch_begin(self, epoch, logs=None):
        dummy      =100
        z, z_lab   =z_sample([dummy,self.model.params[0][7]], self.model.params[2][0], True)
        x          =self.model([z,z_lab]).numpy()
        if self.model.params[0][0] =='MNIST' :
         size      =int(np.sqrt(x.shape[-1]))
         x         =np.reshape(x, (-1,size,size))
         figure    =self.plotter  ('collage', x, ['gray'])         
        else:
         if self.model.params[0][0]=='rfant3':          
          fobj,keys=rHDF(os.path.join(self.model.paths[1],self.model.params[0][0]+'.h5'))
          X, _     =scale_vector_features(fobj['features'][:,:-1])
          figure   =self.plotter('box', [x,X] ,[])
        image      =self.converter(figure)
        with self.writer.as_default():
         tf.summary.image("GAN Predictions", image, step=epoch)
    
    def on_train_end(self, logs=None):
        self.emdvis(self.model,self.model.params[2][0])


class callback_custom_history(tf.keras.callbacks.Callback):
    
    def __init__(self, plotter, whdf):
        super().__init__()
        self.plotter=plotter
        self.whdf   =whdf
    
    def on_train_begin(self, logs=None):
        self.metric_key  =[]
        self.metric_one  =[]
        self.metric_two  =[]
        self.metric_three=[]
        self.metric_four =[]

    def on_train_batch_end(self, batch, logs=None):
        keys=list(logs.keys())
        self.metric_one.append  (logs[keys[0]])
        self.metric_two.append  (logs[keys[1]])
        self.metric_three.append(logs[keys[2]])
        self.metric_four.append (logs[keys[3]])
        if batch==0:
         self.metric_key.append(      keys[0] )
         self.metric_key.append(      keys[1] )
         self.metric_key.append(      keys[2] )
         self.metric_key.append(      keys[3] )

    def on_train_end(self, logs=None):
        sfn=os.path.join(self.model.paths[5],'train','metrics')
        self.whdf(sfn+'.h5'                                                                       ,
                  [self.metric_key[0], self.metric_key[1], self.metric_key[2], self.metric_key[3]],
                  [self.metric_one   , self.metric_two   , self.metric_three , self.metric_four  ] )
        fig=self.plotter('metrics'                                                                          ,
                         [[self.metric_key[0], self.metric_key[1], self.metric_key[2], self.metric_key[3]],
                          [self.metric_one   , self.metric_two   , self.metric_three , self.metric_four  ] ],
                         [[self.model.params [0][6]                                                      ],
                          ['lightsteelblue'  , 'cornflowerblue'  ,'b'                ,'r'                ] ] )
        plt.savefig(sfn+'.png',format='png')


