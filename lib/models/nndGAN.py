####################################
### UNIVERSITY OF NEW MEXICO     ###
### RF & ANTENNA LAB             ###
### GANs FOR DEVICE OPTIMIZATION ###
### MODEL & CALLBACK DEFINITIONS ###
### by: OAMEED NOAKOASTEEN       ###
####################################

import os
import numpy                    as  np
import tensorflow               as  tf
from   matplotlib import pyplot as plt
from   utilsd     import           (rHDF                 ,
                                    scale_vector_features )

##############
### LAYERS ###
##############

def layers_fc(X,OUTSIZE,NAME, BATCHNORM=True,LINEAR=False):
 initializer=tf.keras.initializers.he_normal   () 
 x          =tf.keras.layers.Dense             (OUTSIZE, activation=None, name=NAME      ,kernel_initializer=initializer)(X)
 if     BATCHNORM:
  x         =tf.keras.layers.BatchNormalization(                          name=NAME+'_bn'                               )(x)
 if not LINEAR   :
  x         =tf.keras.layers.ReLU              ()                                                                        (x)
 return x
 
#############
### MODEL ###
#############
def def_discriminator(X): 
 x=layers_fc(X,512  ,'flc_1', False      )
 x=layers_fc(x,256  ,'flc_2', False      )
 x=layers_fc(x,128  ,'flc_3', False      )
 x=layers_fc(x,1    ,'flc_4', False, True)
 return x

def def_generator(X,SHAPE):
 x=layers_fc(X,128  ,'flc_5'             )
 x=layers_fc(x,256  ,'flc_6'             )
 x=layers_fc(x,512  ,'flc_7'             )
 x=layers_fc(x,SHAPE,'flc_8', True , True)
 x=tf.keras.layers.Lambda(tf.keras.activations.sigmoid)(x)
 return x

def z_sample(SHAPE):
 return tf.random.normal(SHAPE) 

def loss_wasserstein_disc(REAL,FAKE,PENALTY,LAMBDA):
 loss_mean_real=tf.reduce_mean(tf.math.multiply(REAL,-tf.ones_like (REAL)))
 loss_mean_fake=tf.reduce_mean(tf.math.multiply(FAKE, tf.ones_like (FAKE)))
 loss          =loss_mean_real+loss_mean_fake+LAMBDA*PENALTY
 return loss, loss_mean_real, loss_mean_fake 

def loss_wasserstein_genr(     FAKE):
 loss_mean_fake=tf.reduce_mean(tf.math.multiply(FAKE,-tf.ones_like (FAKE)))
 return loss_mean_fake

class GAN(tf.keras.Model):

    def __init__(self, discriminator, generator, params, paths):
        super().__init__()
        self.discriminator  =discriminator
        self.generator      =generator
        self.params         =params
        self.paths          =paths

    def compile(self, d_optimizer, g_optimizer, loss_disc, loss_genr):
        super().compile()
        self.d_optimizer    =d_optimizer
        self.g_optimizer    =g_optimizer
        self.loss_disc      =loss_disc
        self.loss_genr      =loss_genr

    def call(self, X, training=None):
        x=self.generator(X, training=True)        
        return x

    def train_step(self, data):
        
        LAMBDA               =10
        loss_disc_real_temp  =[]
        loss_disc_fake_temp  =[]
        
        if isinstance(data, tuple):        
         images_real         =data[0]
        
        for i in range(5):
         with tf.GradientTape() as tape_disc:
          Z                   =z_sample          ([self.params[0][3], self.params[0][7]]                   )
          images_generated    =self.generator    (Z                 , training=True                        )
          logit_real          =self.discriminator(images_real       , training=True                        )
          logit_fake          =self.discriminator(images_generated  , training=True                        )
          
          alpha               =tf.random.normal([self.params[0][3],1])                                       # BEGIN CALCULATING GRADIENT PENALTY
          delta               =images_generated-images_real
          interpolates        =images_real+(alpha*delta)
          logit_interpolates  =self.discriminator(interpolates      , training=True                  )
          grads_penalty       =tf.gradients(logit_interpolates, [interpolates]                       )[0]
          slopes              =tf.math.sqrt(tf.math.reduce_sum(tf.math.square(grads_penalty), axis=1))
          gradient_penalty    =tf.reduce_mean((slopes-1)**2)                                                 # END  CALCULATING GRADIENT PENALTY
          
          loss_disc,lmr,lmf   =self.loss_disc    (logit_real        , logit_fake, gradient_penalty, LAMBDA )
          loss_disc_real_temp.append(lmr)                                                                    # DISCRIMINATOR'S LOSS: FROM REAL IMAGES
          loss_disc_fake_temp.append(lmf)                                                                    # DISCRIMINATOR'S LOSS: FROM FAKE IMAGES
         grads_disc           =tape_disc.gradient(    loss_disc     , self.discriminator.trainable_weights )
         self.d_optimizer.apply_gradients        (zip(grads_disc    , self.discriminator.trainable_weights))
                                
        with tf.GradientTape() as tape_genr:
         Z                    =z_sample          ([self.params[0][3], self.params[0][7]]                   )
         images_generated     =self.generator    (Z                 , training=True                        )
         logit_fake           =self.discriminator(images_generated  , training=True                        )
         loss_genr            =self.loss_genr    (                    logit_fake                           )
        grads_genr            =tape_genr.gradient(    loss_genr     , self.generator.trainable_weights     )
        self.g_optimizer.apply_gradients         (zip(grads_genr    , self.generator.trainable_weights    ))
        
        return {"d_loss_real": tf.reduce_mean(loss_disc_real_temp), 
                "d_loss_fake": tf.reduce_mean(loss_disc_fake_temp),
                "d_loss"     : loss_disc                          ,
                "g_loss"     : loss_genr                          ,
                "penalty"    : LAMBDA*gradient_penalty             }

##########################
### TRAINING CALLBACKS ###
##########################
class callback_custom_ckpt(tf.keras.callbacks.Callback):
    
    def on_epoch_end(self, epoch, logs=None):
        tf.keras.models.save_model(self.model ,self.model.paths[4])

class callback_custom_monitor(tf.keras.callbacks.Callback):

    def __init__(self, writer, plotter, converter):
        super().__init__()
        self.writer   =writer
        self.plotter  =plotter
        self.converter=converter

    def on_epoch_begin(self, epoch, logs=None):
        dummy  =100
        z      =z_sample([dummy, self.model.params[0][7]])
        x      =self.model(z).numpy()
        if self.model.params[0][0]=='MNIST':
         size  =int(np.sqrt(x.shape[-1]))
         x     =np.reshape(x, (-1,size,size))
         figure=self.plotter  ('collage', x, ['gray'])
        else:
         if self.model.params[0][0] in ['rfant1','rfant2']:          
          fobj,keys=rHDF(os.path.join(self.model.paths[1],self.model.params[0][0]+'.h5'))
          X, _     =scale_vector_features(fobj['features'][:,:-1])
          figure   =self.plotter('box', [x,X] ,[])
        image =self.converter(figure)
        with self.writer.as_default():
         tf.summary.image("GAN Generated Images", image, step=epoch)

class callback_custom_history(tf.keras.callbacks.Callback):
    
    def __init__(self, plotter, whdf):
        super().__init__()
        self.plotter=plotter
        self.whdf   =whdf
    
    def on_train_begin(self, logs=None):
        self.metric_key  =[]
        self.metric_one  =[]
        self.metric_two  =[]
        self.metric_three=[]
        self.metric_four =[]
        self.metric_five =[]

    def on_train_batch_end(self, batch, logs=None):
        keys=list(logs.keys())
        self.metric_one.append  (logs[keys[0]])
        self.metric_two.append  (logs[keys[1]])
        self.metric_three.append(logs[keys[2]])
        self.metric_four.append (logs[keys[3]])
        self.metric_five.append (logs[keys[4]])
        if batch==0:
         self.metric_key.append(      keys[0] )
         self.metric_key.append(      keys[1] )
         self.metric_key.append(      keys[2] )
         self.metric_key.append(      keys[3] )
         self.metric_key.append(      keys[4] )
    def on_train_end(self, logs=None):
        sfn=os.path.join(self.model.paths[5],'train','metrics')
        self.whdf(sfn+'.h5'                                                                                           ,
                  [self.metric_key[0], self.metric_key[1], self.metric_key[2], self.metric_key[3], self.metric_key[4]],
                  [self.metric_one   , self.metric_two   , self.metric_three , self.metric_four  , self.metric_five  ] )
        fig=self.plotter('metrics'                                                                                              ,
                         [[self.metric_key[0], self.metric_key[1], self.metric_key[2], self.metric_key[3], self.metric_key[4]],
                          [self.metric_one   , self.metric_two   , self.metric_three , self.metric_four  , self.metric_five  ] ],
                         [[self.model.params [0][6]                                                                          ],
                          ['lightsteelblue'  , 'cornflowerblue'  ,'b'                ,'r'                ,'magenta'          ] ] )
        plt.savefig(sfn+'.png',format='png')


