####################################
### UNIVERSITY OF NEW MEXICO     ###
### RF & ANTENNA LAB             ###
### GANs FOR DEVICE OPTIMIZATION ###
### PARAMETER DEFINITIONS        ###
### by: OAMEED NOAKOASTEEN       ###
####################################

import os
import argparse

### DEFINE PARSER
parser=argparse.ArgumentParser() 

### DEFINE PARAMETERS
parser.add_argument('-data', type=str  ,                help='NAME-OF-DATA-TYPE-DIRECTORY',required=True)
parser.add_argument('-pt'  , type=int  ,                help='PROBLEM TYPE: GAN/CGAN'     ,required=True)
parser.add_argument('-net' , type=str  , default='v00', help='NAME-OF-NETWORK-PROJECT'                  )
parser.add_argument('-sc'  , type=int  , default=1    , help='DATA SCALING SCHEME '                     )
  # GROUP: HYPER-PARAMETERS FOR TRAINING
parser.add_argument('-b'   , type=int  , default=128  , help='BATCH-SIZE'                               )
parser.add_argument('-epc' , type=int  , default=50   , help='NUMBER-OF-TRAINING-EPOCHS'                )
parser.add_argument('-bc'  , type=int  , default=10000, help='BATCH-BUFFER-CAPACITY'                    )
parser.add_argument('-step', type=int  , default=100  , help='EVERY THIS MANY STEPS DO SOMETHING'       )
parser.add_argument('-vs'  , type=float, default=0.1  , help='VALIDATION SPLIT '                        )
 # GROUP:  HYPER-PARAMETERS FOR ARCHITECTURES
parser.add_argument('-zd'  , type=int  , default=100  , help='GAN LATENT DIMENSION'                     )

### ENABLE FLAGS
args=parser.parse_args()

### CONSTRUCT PARAMETER STRUCTURES
PATHS  =[os.path.join('..','..','data'    ,args.data,'raw'        ),
         os.path.join('..','..','data'    ,args.data,'processed'  ),
         os.path.join('..','..','data'    ,args.data,'info'       ),
         os.path.join('..','..','networks',args.net ,'tfrecords'  ),
         os.path.join('..','..','networks',args.net ,'checkpoints'),
         os.path.join('..','..','networks',args.net ,'logs'       ),
         os.path.join('..','..','networks',args.net ,'predictions') ]

if  args.pt==0:
 problemtype ='gan'
else:
 if args.pt==1:
  problemtype='cgan'

PARAMS =[[args.data  ,
          args.net   ,
          args.sc    ,
          args.b     ,
          args.epc   ,
          args.bc    ,
          args.step  ,
          args.zd    ,
          args.vs    ,
          problemtype ]]

if  args.data  == 'rfant1'  :
 PARAMS.append  ([10])
 PARAMS.append  ([  ])
else:
 if args.data  == 'rfant2'  :
  PARAMS.append ([10])
  PARAMS.append ([  ])
 else:
  if args.data == 'rfant3'  :
   PARAMS.append ([10])
   PARAMS.append ([2 ])
  else:
   if args.data in ['MNIST']:
    PARAMS.append([28,28,1])
    PARAMS.append([10     ])

# PARAMS        [0][0] : DATASET TYPE
# PARAMS        [0][1] : NETWORK DIRECTORY
# PARAMS        [0][2] : DATA SCALING (AND DTYPE CONVERSION) SCHEME: '1': SCALE TO [0,1], '2': SCALE TO [-1,1]
# PARAMS        [0][3] : BATCH SIZE
# PARAMS        [0][4] : TRAINING EPOCHS
# PARAMS        [0][5] : BATCH BUFFER CAPACITY
# PARAMS        [0][6] : EVERY THIS MANY STEPS DO SOMETHING
# PARAMS        [0][7] : GAN's LATENT DIMENSION
# PARAMS        [0][8] : VALIDATION SPLIT RATIO
# PARAMS        [0][9] : PROBLEM TYPE: GAN (0) / CGAN (1)
# PARAMS        [1][*] : DATASET SHAPE
# PARAMS        [2][0] : NUMBER OF CATEGORIES


