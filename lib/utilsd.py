####################################
### UNIVERSITY OF NEW MEXICO     ###
### RF & ANTENNA LAB             ###
### GANs FOR DEVICE OPTIMIZATION ###
### UTILITY FUNCTION DEFINITIONS ###
### by: OAMEED NOAKOASTEEN       ###
####################################

import os
import io
import numpy                    as np
import tensorflow               as tf
import h5py
from   matplotlib import pyplot as plt

def cleanup(PATH):
 for FILE in os.listdir(PATH):
  if not FILE.startswith('.'):   
   FILE_PATH=os.path.join(PATH,FILE)
   os.remove(FILE_PATH)

def getFILENAMES(PATH,COMPLETE=True):
 fn=[]
 for FILE in os.listdir(PATH):
  if COMPLETE:
   if not FILE.startswith('.'):
    fn.append(os.path.join(PATH,FILE))
  else:
   if not FILE.startswith('.'):
    fn.append(FILE)
 return fn

def tar_uncompress(FILENAME,PATH):
 import tarfile
 with tarfile.open(FILENAME,'r') as tar:
  tar.extractall(path=PATH)

def wHDF(FILENAME,DIRNAMES,DATA):
 import h5py
 fobj=h5py.File(FILENAME,'w')
 for i in range(len(DIRNAMES)):
  fobj.create_dataset(DIRNAMES[i],data=DATA[i])
 fobj.close()

def rHDF(FILENAME):
 fobj=h5py.File(FILENAME,'r')
 keys=[key for key in fobj.keys()]
 return fobj,keys

def wFILE(SAVEPATH,FILENAME,DATA):
 with open(os.path.join(SAVEPATH,FILENAME), "w") as fobj:
  for data in DATA:
    fobj.write("{}\n".format(data))

def wCSV(FILENAME,DATA):
 np.savetxt(FILENAME, DATA, fmt='%.4e', delimiter=',')

def rCSV(PATH,FILENAME):
 return np.loadtxt(os.path.join(PATH,FILENAME), delimiter=',')

def check_label_is_correct(IMG,LAB):
 for i in range(IMG.shape[0]):
  if IMG[i][-1] < 40:
   label =0
  else:
   if np.logical_and(IMG[i][-1] > 40, IMG[i][-1] < 80):
    label=1
  if not LAB[i]==label:
   print(' WARNING! LABEL MISMATCH! ')
   exit()
 
def plotter(MODE,DATA,CONFIG): 
 if   MODE=='collage':
  # CONFIG[0]: COLORMAPR
  size=10
  fig =plt.figure()
  for  i in range(size):
   for j in range(size):
    ax=plt.subplot(size,size,i*size+(j+1))
    ax.imshow     (DATA[i*size+j], cmap=CONFIG[0])
    ax.axis       ('off')
  plt.subplots_adjust(left=0,right=1,bottom=0,top=1,wspace=0,hspace=0.075)
 else:
  if MODE=='metrics':
   # CONFIG[0]: COLOR CODES
   idx   =[i for i in range(len(DATA[1][0]))][0:-1]                   # :CONFIG[0][0]
   fig,ax=plt.subplots()
   for i in range(len(DATA[0])):    
    data =np.array(DATA[1][i])[idx]
    index=(np.array(idx)+1)/1e3
    ax.plot(index, data, CONFIG[1][i], linewidth=2, label=DATA[0][i]) # /np.amax(data)
                                                                      # ax.set_ylim([0,1])
   ax.set_xlim([0,index[-1]])
   ax.set_title ('Training Loss' )
   ax.set_xlabel('Iterations (K)')
   ax.legend    ()
   ax.grid      ()
  else:
   if MODE=='box':
    def draw_plot(data, offset, edge_color):
     pos  =np.arange (data.shape[1])+offset 
     bp   =ax.boxplot(data, positions=pos, widths=0.3)
     for element in ['boxes', 'whiskers', 'fliers', 'medians', 'caps']:
      plt.setp(bp[element], color=edge_color)
     return bp['boxes'][0]
    fig,ax =plt.subplots()
    legend1=draw_plot(DATA[0], -0.2, 'k')
    legend2=draw_plot(DATA[1], +0.2, 'r')
    plt.xticks(ticks=range(DATA[0].shape[1]),labels=[i+1 for i in range(DATA[0].shape[1])])
    ax.legend([legend1,legend2],['Predictions','Training Dataset'], loc='upper right')
 return fig

def plot_to_image(figure):
 buf  =io.BytesIO()
 plt.savefig(buf, format='png')
 plt.close  (figure) 
 buf.seek   (0)
 image=tf.image.decode_png(buf.getvalue(), channels=0)
 image=tf.expand_dims     (image         ,          0)
 return image

def embedding_visulaizer(MODEL,NUMCLASS):
 from tensorboard.plugins import projector
 logdir    =os.path.join(MODEL.paths[5],'train','embedding')
 weights   =tf.Variable (MODEL.embedding.layers[1].get_weights()[0])
 if not os.path.exists(logdir):
  os.makedirs(logdir)
 wFILE(logdir, 'metadata'+'.tsv', [str(i) for i in range(NUMCLASS)])
 checkpoint=tf.train.Checkpoint(embedding=weights)
 checkpoint.save(os.path.join(logdir, 'embedding'+'.ckpt'))
 config    = projector.ProjectorConfig()
 embedding = config.embeddings.add()
 embedding.tensor_name  = 'embedding/.ATTRIBUTES/VARIABLE_VALUE'
 embedding.metadata_path= 'metadata'+'.tsv'
 projector.visualize_embeddings(logdir, config)

def scale_vector_features(X):
 minval =np.amin(X,axis=0)
 maxval =np.amax(X,axis=0)
 x      =np.array([X[i] for i in range(X.shape[0])])
 for i in range(X.shape[1]):
  x[:,i]=(x[:,i]-minval[i])/(maxval[i]-minval[i])
 vals   =[minval,maxval]
 return x, vals

def scale_up_vector_features(X,VALS):
 x      =np.array([X[i] for i in range(X.shape[0])])
 minvals=VALS[0]
 maxvals=VALS[1]
 for i in range(X.shape[1]):
  x[:,i]=(maxvals[i]-minvals[i])*x[:,i]+minvals[i]
 return x


