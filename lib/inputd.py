####################################
### UNIVERSITY OF NEW MEXICO     ###
### RF & ANTENNA LAB             ###
### GANs FOR DEVICE OPTIMIZATION ###
### INPUT FUNCTION DEFINITIONS   ###
### by: OAMEED NOAKOASTEEN       ###
####################################

import os
import numpy                          as np
import tensorflow                     as tf
from   utilsd     import (getFILENAMES          ,
                          wCSV                  ,
                          scale_vector_features  )
from   datasetd   import  get_dataset

def get_data(PATHS,PARAMS,MODE):
 def process_scale_dtype(X,TYPE):
  if TYPE   ==1:
   maxval   =np.amax(X)
   x        =X*(1/maxval)
   vals     =[maxval]
  else:
   if TYPE  ==2:
    maxval  =np.amax(X)
    x       =(X-(maxval/2))/(maxval/2)
    vals    =[maxval]
   else:
    if TYPE ==3:
     x, vals=scale_vector_features(X)
  return x.astype('float32'), vals
 img,lab   =get_dataset(PARAMS[0][0],PATHS,MODE)
 img,vals  =process_scale_dtype(img,PARAMS[0][2])
 if PARAMS[0][0] in ['rfant1','rfant2','rfant3']:
  minfilename=os.path.join(PATHS[2],'scale'+'_min')
  maxfilename=os.path.join(PATHS[2],'scale'+'_max')
  wCSV(minfilename+'.csv',vals[0])
  wCSV(maxfilename+'.csv',vals[1])
 return img, lab

def saveTFRECORDS(PATHS, PARAMS, MODE):
 def get_train_validation_splits(IMG,LAB,SPLIT):
  rng       =np.random.default_rng()
  indeces   =[i for i in range(IMG.shape[0])]
  rng.shuffle(indeces)
  img       =IMG[indeces]
  lab       =LAB[indeces]
  img_splits=np.split(img,[int(img.shape[0]*(1-SPLIT))])
  lab_splits=np.split(lab,[int(img.shape[0]*(1-SPLIT))])
  return img_splits[0], lab_splits[0], img_splits[1], lab_splits[1]
 def serialize_example(IMG,LAB):  
  feature      ={'img'    : tf.train.Feature(bytes_list=tf.train.BytesList(value=[IMG.tostring()])), 
                 'lab'    : tf.train.Feature(int64_list=tf.train.Int64List(value=[LAB           ])) }
  example_proto=tf.train.Example(features=tf.train.Features(feature=feature))
  return example_proto.SerializeToString()
 def write_serialized_example(IMG,LAB,MODE):
  filename =os.path.join(PATHS[3],MODE,'batch'+'_'+str(0)+'.tfrecords')
  with tf.io.TFRecordWriter(filename) as writer:
   for i in range(IMG.shape[0]):
    example=serialize_example(IMG[i],LAB[i])
    writer.write(example)
 feature,lab=get_data(PATHS,PARAMS,MODE)
 if MODE=='train':
  feature_train, lab_train, feature_validation, lab_validation=get_train_validation_splits(feature,lab,PARAMS[0][8])
  write_serialized_example(feature_train     , lab_train     , 'train'     ) 
  write_serialized_example(feature_validation, lab_validation, 'validation')
 else:
  if MODE=='test':
   write_serialized_example(feature,lab,'test')

def inputTFRECORDS(PATH, PARAMS):
 SHAPE     =PARAMS[1]
 filenames =getFILENAMES(PATH)
 feature   ={'img'    : tf.io.FixedLenFeature([],tf.string), 
             'lab'    : tf.io.FixedLenFeature([],tf.int64 ) }
 def parse_function(example_proto):
  parsed_example=tf.io.parse_single_example(example_proto,feature )
  img           =tf.io.decode_raw(parsed_example['img'],tf.float32)
  img.set_shape([np.prod(SHAPE)])
  lab           =parsed_example['lab']
  return img,lab
 dataset =tf.data.TFRecordDataset(filenames                                    )
 dataset =dataset.map            (parse_function                               )
 dataset =dataset.batch          (PARAMS[0][3]  , drop_remainder          =True)
 dataset =dataset.shuffle        (PARAMS[0][5]  , reshuffle_each_iteration=True)
 return dataset


