# Antenna Design using a GAN-based Synthetic Data Generation Approach

This is the official repository for the following work published in [_IEEE Open Journal of Antennas and Propagation (OJAP)_](https://www.ieeeaps.org/ieee-ojap):  
* [2022-OJAP](https://ieeexplore.ieee.org/document/9763831): Noakoasteen. Vijayamohanan. Gupta. Christodoulou. _**"Antenna Design using a GAN-based Synthetic Data Generation Approach"**_

This work, in an earlier stage of its development, has also been presented at the following conference:
* [2020 APS/URSI](https://2020apsursi.org/Papers/ViewPaper.asp?PaperNum=2977): On Antenna Q-factor Characterization with Generative Adversarial Networks [\[Conference Paper from IEEE Xplore\]](https://ieeexplore.ieee.org/document/9329612)  

## Acknowledgements

* We would like to thank the University of New Mexico Center for Advanced Research Computing [(CARC)](http://carc.unm.edu/), supported in part by the National Science Foundation, for providing the high performance computing resources used in this work.

## References

_Similar Works_  

[[ 1 ].](https://arc.aiaa.org/doi/10.2514/6.2020-3185) 2020. Yilmaz. German. _Conditional GAN framework for airfoil inverse design_  
[[ 2 ].](https://ieeexplore.ieee.org/document/9020794) 2019. Hodge. Mishra. Zaghloul. _Rf metasurface array design using deep convolutional GANs_  
[[ 3 ].](https://pubs.acs.org/doi/10.1021/acs.nanolett.9b01857) 2019. Jiang. Fan. _Global optimization of dielectric metasurfaces using a physics-driven neural network_  
[[ 4 ].](https://pubs.acs.org/doi/abs/10.1021/acsnano.9b02371) 2019. Jiang. Sell. Hoyer. _Free-form diffractive metagrating design based on GANs_  
[[ 5 ].](https://pubs.acs.org/doi/10.1021/acsphotonics.7b01377) 2018. Liu. Tan. Khoram. _Training deep neural networks for the inverse design of nanophotonic structures_  
[[ 6 ].](https://pubs.acs.org/doi/abs/10.1021/acs.nanolett.8b03171) 2018. Liu. Zhu. Rodrigues. _Generative model for the inverse design of metasurfaces_  

_Deep Learning Algorithms and Architectures_

[[ 7 ].](https://arxiv.org/abs/1704.00028) 2017. Gulrajani. _Improved Training of Wasserstein GANs_  
[[ 8 ].](https://arxiv.org/abs/1701.07875) 2017. Arjovsky. _Wasserstein GAN_  
[[ 9 ].](https://arxiv.org/abs/1411.1784)  2014. Mirza. Osindero. _Conditional Generative Adversarial Nets_  
[[10].](https://arxiv.org/abs/1406.2661)  2014. Goodfellow. _Generative Adversarial Nets_  

_Antenna_

[[11].](https://ieeexplore.ieee.org/document/8888388) 2019. Vijayamohanan. Ayoub. Patriotis. _Peel-off and stick antennas for small unmanned aerial vehicles_  

_Useful Web Tutorials_

[[12].](https://www.coursera.org/lecture/build-basic-generative-adversarial-networks-gans/welcome-to-week-3-veMjm) Zhou. Wasserstein GANs with Gradient Penalty: Week 3 of Coursera's Build Basic Generative Adversarial Networks (GANs)  
[[13].](https://ai.stackexchange.com/questions/29904/what-is-lipschitz-constraint-and-why-it-is-enforced-on-discriminator) hanugm. What is Lipschitz constraint and why it is enforced on discriminator?  
[[14].](https://machinelearningmastery.com/how-to-code-a-wasserstein-generative-adversarial-network-wgan-from-scratch/) Brownlee. How to Develop a Wasserstein GAN (WGAN) From Scratch  
[[15].](https://machinelearningmastery.com/practical-guide-to-gan-failure-modes/) Brownlee. How to Identify and Diagnose GAN Failure Modes  
[[16].](https://machinelearningmastery.com/how-to-develop-a-conditional-generative-adversarial-network-from-scratch/) Brownlee. How to Develop a Conditional GAN (cGAN) From Scratch   
[[17].](https://machinelearningmastery.com/how-to-develop-a-generative-adversarial-network-for-an-mnist-handwritten-digits-from-scratch-in-keras/) Brownlee. How to Develop a GAN for Generating MNIST  
[[18].](https://machinelearningmastery.com/how-to-code-generative-adversarial-network-hacks/) Brownlee. How to Implement GAN Hacks in Keras to Train Stable Models  
[[19].](https://lilianweng.github.io/lil-log/2017/08/20/from-GAN-to-WGAN.html) Weng. _From GAN to WGAN_  

## Code Statistics

<pre>
github.com/AlDanial/cloc v 1.74  T=0.07 s (291.7 files/s, 25235.0 lines/s)
-------------------------------------------------------------------------------
Language                     files          blank        comment           code
-------------------------------------------------------------------------------
Python                          10            146            129            886
YAML                             2              2              0            246
MATLAB                           2             34             17            111
Markdown                         1             32              0             88
Bourne Shell                     6             40              7             79
-------------------------------------------------------------------------------
SUM:                            21            254            153           1410
-------------------------------------------------------------------------------
</pre>

## How to Run

* This project uses [Tensorflow](https://www.tensorflow.org/) Version 2.2.0. The `YAML` files for creating the conda environments used to run this project is included in `run/conda`  

* Experiments `v0x` were conducted to validate the train-to-prediction pipeline for this project:  
  * on the HPC system:
    1. `cd` to the project main directory  
    2. `cd ./run/slurm/`
    3. `sbatch train_v0x.sh`

* Experiment `v11` trains a Conditional GAN (CGAN):  
  * on the local PC:
    1. `cd` to the project main directory and do: `./run/sh/train_v11.sh`  
    2. `predictions.csv` are placed in `networks/v11/predictions/`
    3. use `run/gan/get_cst_Q.m` to simulate the predicted designs and calculate the Q-factor
    4. place the calculated Q-factors `classes.csv` in `networks/v11/predictions/`
    5. `cd` to the project main directory and do: `./run/sh/accuracy_v11.sh`

* Experiments `v2x` train multiple Vanilla GANs, each with a different dataset:
  * on the local PC:
    1. `cd` to the project main directory and do: `./run/sh/train_v2x.sh`  
    2. `predictions.csv` are placed in `networks/v2x/predictions/`
    3. use `run/gan/get_cst_Q.m` to simulate the predicted designs and calculate the Q-factor
    4. place the calculated Q-factors `classes.csv` in `networks/v2x/predictions/`
    5. `cd` to the project main directory and do: `./run/sh/accuracy_v2x.sh`
    6. accuracy plot will be placed in the directory of the last experiment

## Experiment v11

|     |     |     |
|:---:|:---:|:---:|
![][fig_1_1] | ![][fig_1_2] | ![][fig_1_3]

[fig_1_1]:networks/v11/logs/train/metrics.png
[fig_1_2]:networks/v11/predictions/predictions_scaled_up.png
[fig_1_3]:networks/v11/predictions/predaccuracy.svg

## Experiments v2x

|     |     |     |
|:---:|:---:|:---:|
![][fig_21_1] | ![][fig_21_2] | 
![][fig_22_1] | ![][fig_22_2] | ![][fig_22_3]

[fig_21_1]:networks/v21/logs/train/metrics.png
[fig_21_2]:networks/v21/predictions/predictions_scaled_up.png
[fig_22_1]:networks/v22/logs/train/metrics.png
[fig_22_2]:networks/v22/predictions/predictions_scaled_up.png
[fig_22_3]:networks/v22/predictions/predaccuracy.svg




## Experiments v0x
These are validations of the GAN and CGAN architectures using MNIST dataset.

|     |     |
|:---:|:---:|
| **Wasserstein GAN with Gradient Penalty**                       |                                                            |
|<img src="networks/v01/predictions/predictions.png"  width=929/> | <img src="networks/v01/logs/train/metrics.png" width=640/> |
| **Conditional Wasserstein GAN**                                 |                                                            |
|<img src="networks/v02/predictions/predictions.png"  width=929/> | <img src="networks/v02/logs/train/metrics.png" width=640/> |

